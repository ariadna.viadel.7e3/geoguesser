package cat.itb.geoguesser;

import java.util.Arrays;

public class QuestionModel {
    private String question;
    private String capital;



    public QuestionModel(String question, String capital) {
        this.question = question;
        this.capital = capital;

    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }


    @Override
    public String toString() {
        return "QuestionModel{" +
                "question='" + question + '\'' +
                ", capital='" + capital + '\'' +
                '}';
    }
}


