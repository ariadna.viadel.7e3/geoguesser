package cat.itb.geoguesser;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.IllegalFormatCodePointException;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView preguntas;
    private ProgressBar progressBar;
    private TextView textViewprogress;
    private Button option1;
    private Button option2;
    private Button option3;
    private Button option4;
    private Button pista;
    private int pistasrestantes= 3;
    private int contadorquestions = 0;
    private AlertDialog.Builder alertDialog;
    private double score=0;
    private String[] arraypreguntas;
    private String[] arrayrespostas;
    private List<Integer> numerosrandom = new ArrayList<>();
    private ArrayList<QuestionModel> preguntaRespuesta = new ArrayList<>();
    final int NUMPREGUNTAS = 12;
    int posicionarray = 0 ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        preguntas = findViewById(R.id.preguntas);
        progressBar = findViewById(R.id.progressbar);
        textViewprogress = findViewById(R.id.textviewprogress);
        option1 = findViewById(R.id.button1);
        option2 = findViewById(R.id.button2);
        option3 = findViewById(R.id.button3);
        option4 = findViewById(R.id.button4);
        pista = findViewById(R.id.pista);
        progressBar.setMax(120);


        //contenido dentro de res(carpeta)
        Resources resources = getResources();
        arraypreguntas = resources.getStringArray(R.array.arraypreguntas);
        arrayrespostas = resources.getStringArray(R.array.arrayrespuestas);
        for (int i = 0; i <NUMPREGUNTAS ; i++) {
            numerosrandom.add(i);
        }
        Collections.shuffle(numerosrandom);


        for (int i = 0; i <NUMPREGUNTAS ; i++) {
            preguntaRespuesta.add(new QuestionModel(arraypreguntas[i],arrayrespostas[i]));
        }
        reset();
        option1.setOnClickListener(this);
        option2.setOnClickListener(this);
        option3.setOnClickListener(this);
        option4.setOnClickListener(this);
        pista.setOnClickListener(this);




    }

    public void reset(){
        contadorquestions = 0;
        preguntas.setText(preguntaRespuesta.get(numerosrandom.get(contadorquestions)).getQuestion());
        textViewprogress.setText("Question " + (contadorquestions+1) + " of 12");
        textButtons(numerosrandom.get(contadorquestions));
        progressBar.setProgress(10);



    }
    public int retornanumrandom(int position){
        int numerorandomresposta = 0;
        do {
            numerorandomresposta = (int) (Math.random()*NUMPREGUNTAS);

        }while (numerorandomresposta == position);
        return numerorandomresposta;
    }
    public void textButtons(int position){
        int numrandombutton = (int )(( Math.random()*4)+1);

        switch (numrandombutton){
            case 1:
                option1.setText(preguntaRespuesta.get(position).getCapital());
                option2.setText(arrayrespostas[retornanumrandom(position)]);
                option3.setText(arrayrespostas[retornanumrandom(position)]);
                option4.setText(arrayrespostas[retornanumrandom(position)]);
                break;
            case 2:
                option1.setText(arrayrespostas[retornanumrandom(position)]);
                option2.setText(preguntaRespuesta.get(position).getCapital());
                option3.setText(arrayrespostas[retornanumrandom(position)]);
                option4.setText(arrayrespostas[retornanumrandom(position)]);
                break;
            case 3:
                option1.setText(arrayrespostas[retornanumrandom(position)]);
                option2.setText(arrayrespostas[retornanumrandom(position)]);
                option3.setText(preguntaRespuesta.get(position).getCapital());
                option4.setText(arrayrespostas[retornanumrandom(position)]);
                break;
            case 4:
                option1.setText(arrayrespostas[retornanumrandom(position)]);
                option2.setText(arrayrespostas[retornanumrandom(position)]);
                option3.setText(arrayrespostas[retornanumrandom(position)]);
                option4.setText(preguntaRespuesta.get(position).getCapital());
                break;
        }
    }
    public void acabar(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Puntuació");
        builder.setMessage("Puntuació de " + Double.toString(score * 10) + "/ 100");
        builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);

            }
        });
        AlertDialog dialog = builder.create();

        dialog.show();

    }


    public void seguentquestion(){
      contadorquestions++;
      if (contadorquestions > NUMPREGUNTAS-1){
          acabar();

      }else {
          preguntas.setText(preguntaRespuesta.get(numerosrandom.get(contadorquestions)).getQuestion());
          progressBar.incrementProgressBy(10);
          textButtons(numerosrandom.get(contadorquestions));
          textViewprogress.setText("Question " + (contadorquestions+1) + " of 12");
      }
    }

    public boolean correcte(int posicio, int boton){
        boolean correcte= false;
        Button button = null;
        switch (boton){
            case 1:
                button = option1;
                break;
            case 2:
                button = option2;
                break;
            case 3:
                button = option3;
                break;
            case 4:
                button = option4;
                break;

        }
        String respostacorrecta = preguntaRespuesta.get(numerosrandom.get(contadorquestions)).getCapital();
        String respostaintroduida = button.getText().toString() ;
        if (respostaintroduida.equals(respostacorrecta)){
            correcte = true;
            score++;
        }else{
            correcte = false;
            score-=0.5;
        }

        return correcte;
    }
    public void mostraresposta(boolean correcte){
        if (correcte){
            Toast.makeText(MainActivity.this, R.string.correcte, Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(MainActivity.this, R.string.incorrecte, Toast.LENGTH_SHORT).show();

        }
    }


    @Override
    public void onClick(View v) {
        boolean cert = false;
        switch (v.getId()){
            case R.id.button1:
                cert = correcte(contadorquestions,1);
                seguentquestion();
                mostraresposta(cert);
                break;
            case R.id.button2:
                cert = correcte(contadorquestions,2);
                seguentquestion();
                mostraresposta(cert);
                break;
            case R.id.button3:
                cert = correcte(contadorquestions,3);
                seguentquestion();
                mostraresposta(cert);
                break;
            case R.id.button4:
                cert = correcte(contadorquestions,4);
                seguentquestion();
                mostraresposta(cert);
                break;
            case R.id.pista:
                if (pistasrestantes > 0){
                    Toast.makeText(MainActivity.this, "Resposta correcta: " + preguntaRespuesta.get(numerosrandom.get(contadorquestions)).getCapital(), Toast.LENGTH_SHORT).show();
                    seguentquestion();
                    pistasrestantes--;
                    Toast.makeText(MainActivity.this,"Et quedan "+ pistasrestantes+ " pistas restants",Toast.LENGTH_SHORT).show();
                    if (pistasrestantes == 0){
                        pista.setVisibility(View.INVISIBLE);
                    }
                }
                break;
        }

    }

}